package org.fhir.functions.cosmos

import com.google.gson.Gson
import com.mongodb.client.MongoClient
import com.mongodb.client.MongoClients
import com.mongodb.client.model.Filters.eq
import org.fhir.functions.medication.model.Medication

open class MedicationDAO {

    private val url = "mongodb://entity-mongo-dev:QtJVRjtBLvfLqAEqrsyDl93y1VeMCVFTADkH0CovkHQP7NS7CJyTsJOFb7LvN4JsxMcTpwQnbAFO7FWYbUVYZw==@entity-mongo-dev.documents.azure.com:10255/?ssl=true&replicaSet=globaldb"
    private val mongoClient: MongoClient = MongoClients.create(url)
    private val collection = mongoClient.getDatabase("medication").getCollection("medication")

    private val gson = Gson()
    private val paginateLimit = 10

    fun findOne(id: String?): Medication? {
        val doc = collection.find(eq("identifier", id)).first()
        return gson.fromJson(doc!!.toJson(), Medication::class.java)
    }

    fun findAll(): List<Medication> {
        val results = ArrayList<Medication>()
        collection.find().limit(paginateLimit).forEach { doc -> results.add(gson.fromJson(doc.toJson(), Medication::class.java))}
        return results
    }

}