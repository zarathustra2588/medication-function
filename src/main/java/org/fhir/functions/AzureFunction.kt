package org.fhir.functions

import com.microsoft.azure.functions.annotation.*
import com.microsoft.azure.functions.*
import org.fhir.functions.cosmos.MedicationDAO

import java.util.Optional

class AzureFunction {
    /**
     * This function listens at endpoint "/api/hello". Two ways to invoke it using "curl" command in bash:
     * 1. curl -d "HTTP Body" {your host}/api/hello
     * 2. curl {your host}/api/hello?name=HTTP%20Query
     */
    @FunctionName("medication")
    fun medication(
            @HttpTrigger(name = "req", methods = [HttpMethod.GET, HttpMethod.POST], authLevel = AuthorizationLevel.ANONYMOUS)
            request: HttpRequestMessage<Optional<String>>,
            context: ExecutionContext): HttpResponseMessage {
        context.logger.info("Java HTTP trigger processed a request.")

        val id = request.queryParameters["identifier"]
        //val name = request.body.orElse(id)
        val dao = MedicationDAO()

        return if (id != null) {
            request.createResponseBuilder(HttpStatus.OK).body(dao.findOne(id)).build()
        } else {
            request.createResponseBuilder(HttpStatus.OK).body(dao.findAll()).build()
        }
    }
}
