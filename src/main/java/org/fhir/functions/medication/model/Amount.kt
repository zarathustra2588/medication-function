package org.fhir.functions.medication.model

data class Amount(var ratio: String? = null)