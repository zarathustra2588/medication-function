package org.fhir.functions.medication.model

data class Batch(var lotNumber: String? = null,
                 var expirationDate: String? = null)