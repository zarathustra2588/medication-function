package org.fhir.functions.medication.model

/**
 * Medication
 */
data class Medication(var identifier: String? = null,
                      var code: String? = null,
                      var status: String? = null,
                      var manufacturer: String? = null,
                      var form: String? = null,
                      var amount: Amount? = null,
                      var ingredients: String? = null,
                      var batch: Batch? = null)

